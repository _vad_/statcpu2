﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace statCPU2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        float a, b, t;
        int count;
        int o;        
        
        /// <summary>
        /// 1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (o > 0)
            {
                textBox1.Clear();
                o = 0;
            }
            
        textBox1.Text += 1;
        }
        /// <summary>
        /// 2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            if (o > 0)
            {
                textBox1.Clear();
                o = 0;
            }
            textBox1.Text += 2;
        }
        /// <summary>
        /// 3
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            if (o > 0)
            {
                textBox1.Clear();
                o = 0;
            }
            textBox1.Text += 3;
        }
        /// <summary>
        /// 4
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            if (o > 0)
            {
                textBox1.Clear();
                o = 0;
            }
            textBox1.Text += 4;
        }
        /// <summary>
        /// 5
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            if (o > 0)
            {
                textBox1.Clear();
                o = 0;
            }
            textBox1.Text += 5;
        }
        /// <summary>
        /// 6
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button6_Click(object sender, EventArgs e)
        {
            if (o > 0)
            {
                textBox1.Clear();
                o = 0;
            }
            textBox1.Text += 6;
        }
        /// <summary>
        /// 7
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button7_Click(object sender, EventArgs e)
        {
            if (o > 0)
            {
                textBox1.Clear();
                o = 0;
            }
            textBox1.Text += 7;
        }
        /// <summary>
        /// 8
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button8_Click(object sender, EventArgs e)
        {
            if (o > 0)
            {
                textBox1.Clear();
                o = 0;
            }
            textBox1.Text += 8;
        }
        /// <summary>
        /// 9
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button9_Click(object sender, EventArgs e)
        {
            if (o > 0)
            {
                textBox1.Clear();
                o = 0;
            }
            textBox1.Text += 9;
        }
        /// <summary>
        /// 0
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button10_Click(object sender, EventArgs e)
        {
            if (o > 0)
            {
                textBox1.Clear();
                o = 0;
            }
            textBox1.Text += 0;
        }
        /// <summary>
        /// сложение
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button11_Click(object sender, EventArgs e)
        {
            try
            {
                a = float.Parse(textBox1.Text);
                textBox1.Clear();
                count = 1; // +
            }
            catch
            {
                MessageBox.Show("Введи цифры");
            }
        }
        /// <summary>
        /// вычитание
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button12_Click(object sender, EventArgs e)
        {
            try
            { 
                a = float.Parse(textBox1.Text);
                textBox1.Clear();
                count = 2; // -
            }
            catch
            {
                MessageBox.Show("Введи цифры");
            }
        }
        /// <summary>
        /// умножение
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button13_Click(object sender, EventArgs e)
        {
            try
            { 
                a = float.Parse(textBox1.Text);
                textBox1.Clear();
                count = 3; // *
            }
            catch
            {
                MessageBox.Show("Введи цифры");
            }
        }
        /// <summary>
        /// деление
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button14_Click(object sender, EventArgs e)
        {
            try
            { 
                a = float.Parse(textBox1.Text);
                textBox1.Clear();
                count = 4; // /
            }
            catch
            {
                MessageBox.Show("Введи цифры");
            }
        }
        /// <summary>
        /// равно
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button15_Click(object sender, EventArgs e)
        {
                o += 1;
                calculate();

        }
        /// <summary>
        /// запятая
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button16_Click(object sender, EventArgs e)
        {
                textBox1.Text = textBox1.Text + ",";
        }
        /// <summary>
        /// очистка
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button17_Click(object sender, EventArgs e)
        {           
            textBox1.Clear();
        }
        /// <summary>
        /// отрицание
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button18_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                textBox1.Text = "-"; //минус
            }
                else
            {
                t = float.Parse(textBox1.Text);
                t *= -1;
                textBox1.Text = t.ToString();
            }
        }
        /// <summary>
        /// расчет
        /// </summary>
        private void calculate()
        {
            try
            {
                switch (count)
                {
                    case 1:
                        b = a + float.Parse(textBox1.Text);
                        textBox1.Text = b.ToString();
                        break;
                    case 2:
                        b = a - float.Parse(textBox1.Text);
                        textBox1.Text = b.ToString();
                        break;
                    case 3:
                        b = a * float.Parse(textBox1.Text);
                        textBox1.Text = b.ToString();
                        break;
                    case 4:
                        b = a / float.Parse(textBox1.Text);
                        textBox1.Text = b.ToString();
                        break;
                }
            }

            catch
            {
                MessageBox.Show("Ошибка");
            }
        }        
    }
}
